const express = require('express');
const router = express.Router();

const { login, option, followers, convertToXlsx, getLikeFromPost } = require("./controller")
router.get('/', login);
router.post("/option", option);
router.get("/convert", convertToXlsx)
router.post('/getFollowers', followers);
router.post("/likers", getLikeFromPost)


module.exports = router;