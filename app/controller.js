const { user, follower, liker, post } = require("../models")
const puppeteer = require('puppeteer');
let xlsx = require("json-as-xlsx")
const { minimalArgs, blocked_domains, blockedResourceTypes, skippedResources, diff_minutes } = require("./helper")

async function loginUsingPuppeteer(req, res) {
    const browser = await puppeteer.launch({ headless: false, args: minimalArgs })
    const page = await browser.newPage()
    await page.setRequestInterception(true);
    page.on('request', request => {
        const url = request.url()
        if (blocked_domains.some(domain => url.includes(domain)) || blockedResourceTypes.some(resourceType => request.resourceType() === resourceType) || skippedResources.some(resource => url.includes(resource))) {
            request.abort();
        } else {
            request.continue();
        }
    });
    // await page.goto(`https://www.instagram.com/accounts/login/?force_classic_login`)
    await page.goto(`https://www.instagram.com/accounts/login/`)

    try {
        // await page.waitForSelector("form.adjacent", { timeout: 10000 })
        await page.waitForSelector("form.HmktE", { timeout: 10000 })
    } catch (error) {
        await browser.close()
        console.log(error);
        req.session.message = {
            type: "danger",
            message: `Your connection isn't stable`
        }
        return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
    }
    await page.type('input[name="username"]', req.session.user.username)
    await page.type('input[type="password"]', req.session.user.password)
    // await page.click('input[type="submit"]')
    await page.click('button[type="submit"]')
    try {
        await page.waitForSelector("nav.kqZqN.x-Vyw", { timeout: 20000 })
        return [browser, page]
    } catch (error) {
        const errorMessage = await page.evaluate(() => document.querySelector('p[data-testid="login-error-message"]').innerHTML)
        await browser.close()
        console.log(error);
        req.session.message = {
            type: "danger",
            message: errorMessage
        }
        return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })

    }
}

exports.login = (req, res) => res.render('login');

exports.option = (req, res) => {
    if (Object.keys(req.body).length === 0) {
        return res.redirect("/login")
    }
    const { usernameLogin, passwordLogin } = req.body
    req.session.user = { username: usernameLogin, password: passwordLogin }
    res.render('option', { isLoading: req.session.isLoading });
}

exports.followers = async (req, res) => {
    const { username, limitFollowers = 1000 } = req.body
    if (!username) {
        req.session.message = {
            type: "danger",
            message: `Username akun yang ingin discrapping harus dimasukkan`
        }
        return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
    }
    try {
        let followerExist = await follower.findOne({
            order: [['time', 'DESC']],
        })
        followerExist = JSON.parse(JSON.stringify(followerExist))

        // if (followerExist && diff_minutes(new Date(), new Date(followerExist.time)) < 5) {
        //     req.session.message = {
        //         type: "danger",
        //         message: `Please wait 5 minutes before next request`
        //     }
        //     return res.render("option", { message: req.session.message, isLoading : req.session.isLoading })
        // }

        const [browser, page] = await loginUsingPuppeteer(req, res)

        await page.goto("https://www.instagram.com/" + username + "/followers/")
        try {
            await page.waitForSelector("span._ac2a", { timeout: 20000 })
        } catch (error) {
            console.log(error);
            req.session.message = {
                type: "danger",
                message: `Gagal get followers atau username tidak benar`
            }
            return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
        }

        const followersAmount = await page.evaluate(() => document.querySelectorAll('span._ac2a')[1].innerHTML)
        let userExist;
        try {
            userExist = await user.findOne({
                where: { username }
            })
            await user.update({ followersAmount }, { where: { username } })
            if (!(await userExist)) {
                userExist = await user.create({ username, url: `https://www.instagram.com/${username}/`, followersAmount })
                userExist = JSON.parse(JSON.stringify(userExist))
            }
            await page.waitForSelector("span._aacl._aaco._aacw._aacx._aad7._aade", { timeout: 20000 })
        } catch (error) {
            await browser.close()
            console.log(error);
            req.session.message = {
                type: "danger",
                message: `Failed get followers from ${username}`
            }
            return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
        }

        const delay = 10000
        const wait = (ms) => new Promise(res => setTimeout(res, ms));
        const count = async () => await page.evaluate(() => Array.from(document.querySelectorAll("span._aacl._aaco._aacw._aacx._aad7._aade")).length);
        const scrollDown = async () => await page.evaluate(() => document.querySelector("ul._aaey").scrollIntoView(false))

        let preCount = 0;
        let postCount = 0;
        let data = []
        do {
            // const elementPosition = await page.evaluate(() => document.querySelector("div._aano").scrollTop);
            // const heightScrollSection = await page.evaluate(() => document.querySelector("div._aano").offsetHeight);
            // console.log("elementPosition", elementPosition, "heightScrollSection", heightScrollSection);

            // const delay = elementPosition > Number(heightScrollSection) - (Number(heightScrollSection) / 7) ? 15000 : 500

            preCount = Number(await count());
            await scrollDown();
            await wait(delay);
            postCount = Number(await count());

            console.log("preCount", preCount);
            console.log("postCount", postCount);

            let followers = await page.evaluate(() => {
                return Array.from(document.querySelectorAll("span._aacl._aaco._aacw._aacx._aad7._aade")
                ).map(e => e.innerText)
            })

            followers = followers.map(async (e, i) => {
                let followerExist = await follower.findOne({
                    where: { username: e, user_id: userExist.id }
                })
                if (!(await followerExist)) {
                    const time = Number(Date.now())
                    await follower.create({ user_id: userExist.id, username: e, time, url: `https://www.instagram.com/${e}` })
                }
                data[i] = {
                    username: e,
                    url: `https://www.instagram.com/${e}`
                }
            })
        } while (postCount > preCount && postCount < limitFollowers);

        await browser.close()
        res.render('table', { title: "Table Followers", data, linkExcel: `/convert?username=${username}` })
        // res.status(200).json({
        //     message: `success get followers from ${username}`,
        // })
    } catch (error) {
        console.log(error);
        req.session.message = {
            type: "danger",
            message: error.message
        }
        return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
    }
}

exports.convertToXlsx = async (req, res) => {
    try {
        if (req.query.username) {
            let getData = await user.findOne({
                where: { username: req.query.username },
                include: { model: follower }
            })
            getData = JSON.parse(JSON.stringify(getData))
            let followers = getData.followers
            let data = [
                {
                    sheet: "Followers",
                    columns: [
                        { label: "Id", value: "id" },
                        { label: "Username", value: "username" },
                        { label: "Url", value: "url" },
                        { label: "Time", value: "time" }
                    ],
                    content: followers.map((e, i) => {
                        return { id: i + 1, username: e.username, url: e.url, time: new Date(e.time) }
                    })
                },
            ]
            let settings = {
                fileName: `Followers-@${req.query.username}`,
                extraLength: 3,
                writeOptions: {},
            }

            xlsx(data, settings)

            if (res.statusCode === 200) {
                await res.download(`./Followers-@${req.query.username}.xlsx`)
            }
        } else if (req.query.postlink) {
            let getData = await post.findOne({
                where: { url: req.query.postlink },
                include: { model: liker }
            })
            getData = JSON.parse(JSON.stringify(getData))
            let likers = getData.likers
            let data = [
                {
                    sheet: "Likers",
                    columns: [
                        { label: "Id", value: "id" },
                        { label: "Username", value: "username" },
                        { label: "Url", value: "url" },
                        { label: "Time", value: "time" }
                    ],
                    content: likers.map((e, i) => {
                        return { id: i + 1, username: e.username, url: e.url, time: new Date(e.time) }
                    })
                },
            ]
            let settings = {
                fileName: `Likers-postid-${getData.id}`,
                extraLength: 3,
                writeOptions: {},
            }

            xlsx(data, settings)

            if (res.statusCode === 200) {
                await res.download(`./Likers-postid-${getData.id}.xlsx`)
            }
        }
    } catch (error) {
        console.log(error);
        req.session.message = {
            type: "danger",
            message: error.message
        }
        return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
    }
}

exports.getLikeFromPost = async (req, res) => {
    const { link, limitLikers = 1000 } = req.body
    if (!link) {
        req.session.message = {
            type: "danger",
            message: `Url menuju postingan harus dimasukkan`
        }
        return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
    }
    try {
        let likerExist = await liker.findOne({
            order: [['time', 'DESC']],
        })
        likerExist = JSON.parse(JSON.stringify(likerExist))

        // if (likerExist && diff_minutes(new Date(), new Date(likerExist.time)) < 5) {
        //     req.session.message = {
        //         type: "danger",
        //         message: `Please wait 5 minutes before next request`
        //     }
        //     return res.render("option", { message: req.session.message, isLoading : req.session.isLoading })
        // }
        const [browser, page] = await loginUsingPuppeteer(req, res)
        await page.goto(`${link}/liked_by`) //akhir link jangan ada karakter /

        // await page.waitForSelector("a.oajrlxb2.g5ia77u1.qu0x051f.esr5mh6w.e9989ue4.r7d6kgcz.rq0escxv.nhd2j8a9.nc684nl6.p7hjln8o.kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.jb3vyjys.rz4wbd8a.qt6c0cv9.a8nywdso.i1ao9s8h.esuyzwwr.f1sip0of.lzcic4wl._a6hd > div._aacl._aaco._aacw._aacx._aada._aade > span")

        // await page.click("a.oajrlxb2.g5ia77u1.qu0x051f.esr5mh6w.e9989ue4.r7d6kgcz.rq0escxv.nhd2j8a9.nc684nl6.p7hjln8o.kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.jb3vyjys.rz4wbd8a.qt6c0cv9.a8nywdso.i1ao9s8h.esuyzwwr.f1sip0of.lzcic4wl._a6hd > div._aacl._aaco._aacw._aacx._aada._aade > span")

        let postExist;
        try {
            // await page.waitForSelector("div._ab8w._ab94._ab97._ab9f._ab9k._ab9p", { timeout: 20000 })
            await page.waitForSelector("div._ab8w._ab94._ab97._ab9f._ab9k._ab9p._ab9-._aba8", { timeout: 20000 })
            postExist = await post.findOne({ where: { url: link } })
            postExist = JSON.parse(JSON.stringify(postExist))

            if (!(await postExist)) {
                postExist = await post.create({ url: link })
                postExist = JSON.parse(JSON.stringify(postExist))
            }
        } catch (error) {
            await browser.close()
            console.log(error);
            req.session.message = {
                type: "danger",
                message: `Gagal get likers atau link postingan tidak benar`
            }
            return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
        }

        const delay = 10000
        const wait = (ms) => new Promise(res => setTimeout(res, ms));
        // const count = async () => await page.evaluate(() => Array.from(document.querySelectorAll("div._ab8w._ab94._ab97._ab9f._ab9k._ab9p")).length);
        const count = async () => await page.evaluate(() => Array.from(document.querySelectorAll("div._ab8w._ab94._ab97._ab9f._ab9k._ab9p._ab9-._aba8")).length);
        // const scrollDown = async () => await page.evaluate(() => document.querySelector("div.rq0escxv.l9j0dhe7.du4w35lb").scrollIntoView(false))
        const scrollDown = async () => await page.evaluate(() => document.querySelector("main._a993._a995").scrollIntoView(false))
        let preCount = 0;
        let postCount = 0;
        let data = []
        do {

            preCount = Number(await count());
            await scrollDown();
            await wait(delay);
            postCount = Number(await count());

            console.log("preCount", preCount);
            console.log("postCount", postCount);

            let likers = await page.evaluate(() => {
                return Array.from(document.querySelectorAll("div._ab8w._ab94._ab97._ab9f._ab9k._ab9p")
                ).filter((e, i) => i % 2 !== 0).map(e => e.innerText)
            })

            likers = likers.map(async (e, i) => {
                let likerExist = await liker.findOne({
                    where: { username: e, post_id: postExist.id }
                })
                if (!(await likerExist)) {
                    const time = Number(Date.now())
                    await liker.create({ post_id: postExist.id, username: e, time, url: `https://www.instagram.com/${e}` })
                }
                data[i] = {
                    username: e,
                    url: `https://www.instagram.com/${e}`
                }
            })
        } while (postCount > preCount && postCount < limitLikers);

        await browser.close()
        res.render('table', { title: "Table Likers", data, linkExcel: `/convert?postlink=${link}` })
        // res.status(200).json({
        //     message: `success get like from ${link}`,
        // })
    } catch (error) {
        console.log(error);
        req.session.message = {
            type: "danger",
            message: error.message
        }
        return res.render("option", { message: req.session.message, isLoading: req.session.isLoading })
    }
}