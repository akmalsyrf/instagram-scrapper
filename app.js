var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require("dotenv").config();
const session = require("express-session");

var router = require('./app/router');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
  session({
    cookie: {
      maxAge: 1000 /*ms atau 1s*/ * 60 * 60 * 2,
    },
    store: new session.MemoryStore(),
    resave: false,
    saveUninitialized: true,
    secret: "VerySecret",
  })
);

app.use(function (req, res, next) {
  res.locals.message = req.session.message
  req.session.isLoading = false
  delete req.session.message
  next();
});

app.use(router);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.redirect("/")
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page  
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
